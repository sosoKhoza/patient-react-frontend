import axios from 'axios'

const INSTRUCTOR = 'in28minutes'
const COURSE_API_URL = 'http://localhost:8080'
const INSTRUCTOR_API_URL = `${COURSE_API_URL}/patient/${INSTRUCTOR}`

class CourseDataService {

    retrieveAllPatients(name) {
        return axios.get(`${INSTRUCTOR_API_URL}/patients`);
    }
}

export default new PatientDataService()