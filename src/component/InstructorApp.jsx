import React, { Component } from 'react';
import ListPatientComponent from './component/ListPatientComponent';

class InstructorApp extends Component {
    render() {
        return (<>
              <h1>Instructor Application</h1>
              <ListPatientComponent/>
            </>
        )
    }
}

export default InstructorApp